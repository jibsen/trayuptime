/*
 * Tray Up Time
 *
 * Copyright 2009-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <shellapi.h>
#include <psapi.h>

#include "suptime.h"
#include "rsrc.h"

#ifdef _MSC_VER
/* skip C runtime library and set entry to our main function */
#pragma comment(linker,"/ENTRY:entry32")
#endif

const wchar_t *classname = L"TrayUpTimeWindow";
const wchar_t *appname = L"TrayUpTime";

HINSTANCE hInstance;
HMENU hPopupMenu;
HICON hIcon, hIconSm;
NOTIFYICONDATA nid;

struct suptime sut;

void memzero(void *p, size_t len)
{
	unsigned char *cp = (unsigned char *) p;

	while (len--) {
		*cp++ = 0;
	}
}

const wchar_t *plural(unsigned int n)
{
	return n == 1 ? L"" : L"s";
}

INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT wMsg, WPARAM wParam, LPARAM lParam)
{
	switch (wMsg) {
	case WM_CLOSE:
		PostMessage(hDlg, WM_COMMAND, IDOK, 0);
		break;

	case WM_COMMAND:
		if (wParam == IDOK) {
			EndDialog(hDlg, wParam);
		}
		break;

	default:
		return FALSE;
		break;
	}

	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	POINT pt;

	switch (uMsg) {
	case WM_CREATE:
		hPopupMenu = CreatePopupMenu();
		AppendMenu(hPopupMenu, MF_STRING, IDM_ABOUT, L"About");
		AppendMenu(hPopupMenu, MF_STRING, IDM_EXIT, L"Quit");

		memzero(&nid, sizeof(nid));
		nid.cbSize = sizeof(nid);
		nid.hWnd = hWnd;
		nid.uID = IDI_TRAY;
		nid.uFlags = NIF_ICON + NIF_MESSAGE + NIF_TIP;
		nid.uCallbackMessage = WM_SHELLNOTIFY;
		nid.hIcon = hIconSm;
		lstrcpy(nid.szTip, L"TrayUpTime");

		if (!Shell_NotifyIcon(NIM_ADD, &nid)) {
			return -1;
		}
		break;

	case WM_DESTROY:
		DestroyMenu(hPopupMenu);
		Shell_NotifyIcon(NIM_DELETE, &nid);
		PostQuitMessage(0);
		break;

	case WM_COMMAND:
		switch (wParam) {
		case IDM_ABOUT:
			DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_ABOUT), hWnd, AboutDlgProc, 0);
			break;

		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		}
		break;

	case WM_SHELLNOTIFY:
		if (wParam != IDI_TRAY) {
			break;
		}

		switch (lParam) {
		case WM_RBUTTONUP:
			/* show popup menu */
			GetCursorPos(&pt);
			SetForegroundWindow(hWnd);
			TrackPopupMenu(hPopupMenu, TPM_RIGHTALIGN + TPM_RIGHTBUTTON, pt.x, pt.y, 0, hWnd, NULL);
			PostMessage(hWnd, WM_NULL, 0, 0);
			break;

		case WM_LBUTTONDBLCLK:
			/* do about on double-click */
			SendMessage(hWnd, WM_COMMAND, IDM_ABOUT, 0);
			break;

		case WM_MOUSEMOVE:
			/* update notification tooltip */
			{
				unsigned int days, hours, minutes, seconds;
				LONGLONG uptime;

				if (sut_get(&sut, &uptime)) {
					seconds = uptime % 60;
					minutes = (uptime / 60) % 60;
					hours = (uptime / (60 * 60)) % 24;
					days = uptime / (24 * 60 * 60);

					wsprintf(nid.szTip, L"TrayUpTime\n%u day%s %u hour%s %u min%s", days, plural(days), hours, plural(hours), minutes, plural(minutes));
				}
				else {
					wsprintf(nid.szTip, L"TrayUpTime\nUnable to query counter");
				}

				Shell_NotifyIcon(NIM_MODIFY, &nid);
			}
			break;
		}
		break;

	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
		break;
	}

	return 0;
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR CmdLine, int CmdShow)
{
	WNDCLASSEX wc;
	MSG msg;
	HWND hwnd;

	hInstance = hInst;

	hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MAINICON));
	hIconSm = LoadImage(hInstance, MAKEINTRESOURCE(IDI_MAINICON), IMAGE_ICON, GetSystemMetrics(SM_CXSMSIZE), GetSystemMetrics(SM_CYSMSIZE), LR_DEFAULTCOLOR + LR_SHARED);

	memzero(&wc, sizeof(wc));
	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW + CS_VREDRAW + CS_DBLCLKS;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hbrBackground = (HBRUSH) COLOR_APPWORKSPACE;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = classname;
	wc.hIcon = hIcon;
	wc.hIconSm = hIconSm;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);

	RegisterClassEx(&wc);

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, classname, appname, WS_OVERLAPPED + WS_CAPTION + WS_SYSMENU + WS_MINIMIZEBOX + WS_MAXIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, 350, 200, NULL, NULL, hInst, NULL);

	if (hwnd == NULL) {
		return 0;
	}

	/* attempt to reduce memory usage */
	EmptyWorkingSet(GetCurrentProcess());

	/* message loop */
	while (GetMessage(&msg, NULL, 0, 0) > 0) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

int entry32()
{
	HANDLE hMutex;
	DWORD dwRes;

	/* create mutex to check for multiple instances */
	hMutex = CreateMutex(NULL, FALSE, classname);

	dwRes = GetLastError();

	if (dwRes != ERROR_SUCCESS) {
		if (dwRes == ERROR_ALREADY_EXISTS) {
			CloseHandle(hMutex);
		}
		ExitProcess(1);
	}

	if (!sut_open(&sut)) {
		MessageBox(NULL, L"Unable to access performance counter.", appname, MB_OK + MB_ICONERROR);
		ExitProcess(1);
	}

	dwRes = WinMain((HINSTANCE) GetModuleHandle(NULL), NULL, NULL, SW_SHOWDEFAULT);

	sut_close(&sut);

	CloseHandle(hMutex);

	ExitProcess(dwRes);
}
