/*
 * Tray Up Time
 *
 * PDH performance counter query
 *
 * Copyright 2009-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "suptime.h"

#ifdef _MSC_VER
#pragma comment(lib,"pdh.lib")
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif

static BOOL make_counterpath(wchar_t *buffer, DWORD size)
{
	DWORD num, used, left;

	used = 0;
	left = size;

	buffer[used] = L'\\';
	used++;
	left--;

	num = left;
	if (PdhLookupPerfNameByIndex(NULL, 2, &buffer[used], &num) != ERROR_SUCCESS) {
		return FALSE;
	}

	num--;
	used += num;
	left -= num;

	buffer[used] = L'\\';
	used++;
	left--;

	num = left;
	if (PdhLookupPerfNameByIndex(NULL, 674, &buffer[used], &num) != ERROR_SUCCESS) {
		return FALSE;
	}

	return TRUE;
}

BOOL sut_open(struct suptime *sut)
{
	wchar_t path[PDH_MAX_COUNTER_PATH + 1];

	if (sut == NULL) {
		return FALSE;
	}

	if (!make_counterpath(path, ARRAY_SIZE(path))) {
		return FALSE;
	}

	if (PdhValidatePath(path) != ERROR_SUCCESS) {
		return FALSE;
	}

	if (PdhOpenQuery(NULL, 0, &sut->hQuery) != ERROR_SUCCESS) {
		return FALSE;
	}

	if (PdhAddCounter(sut->hQuery, path, 0, &sut->hCounter) != ERROR_SUCCESS) {
		PdhCloseQuery(sut->hQuery);
		return FALSE;
	}

	return TRUE;
}

BOOL sut_get(struct suptime *sut, LONGLONG *v)
{
	PDH_FMT_COUNTERVALUE value;

	if (sut == NULL || v == NULL) {
		return FALSE;
	}

	if (PdhCollectQueryData(sut->hQuery) != ERROR_SUCCESS) {
		return FALSE;
	}

	if (PdhGetFormattedCounterValue(sut->hCounter, PDH_FMT_LARGE, NULL, &value) != ERROR_SUCCESS) {
		return FALSE;
	}

	if (value.CStatus != PDH_CSTATUS_NEW_DATA && value.CStatus != PDH_CSTATUS_VALID_DATA) {
		return FALSE;
	}

	*v = value.largeValue;

	return TRUE;
}

BOOL sut_close(struct suptime *sut)
{
	if (sut == NULL) {
		return FALSE;
	}

	return PdhCloseQuery(sut->hQuery) == ERROR_SUCCESS;
}
