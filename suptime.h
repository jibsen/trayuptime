/*
 * Tray Up Time
 *
 * PDH performance counter query
 *
 * Copyright 2009-2013 Joergen Ibsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SUPTIME_H_INCLUDED
#define SUPTIME_H_INCLUDED

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <pdh.h>
#include <pdhmsg.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PDH_MAX_COUNTER_PATH
#define PDH_MAX_COUNTER_PATH 2048
#endif

/**
 * Structure used to represent System Up Time query.
 * @see sut_open
 */
struct suptime {
	PDH_HQUERY hQuery;
	PDH_HCOUNTER hCounter;
};

/**
 * Create a performance counter query for the System Up Time counter.
 * @param sut pointer to `suptime` structure.
 * @return `TRUE` on success, `FALSE` otherwise.
 */
BOOL sut_open(struct suptime *sut);

/**
 * Collect data from the specified query.
 * @param sut pointer to `suptime` structure.
 * @param v pointer to variable receiving the data.
 * @return `TRUE` on success, `FALSE` otherwise.
 */
BOOL sut_get(struct suptime *sut, LONGLONG *v);

/**
 * Close query.
 * @param sut pointer to `suptime` structure.
 * @return `TRUE` on success, `FALSE` otherwise.
 */
BOOL sut_close(struct suptime *sut);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* SUPTIME_H_INCLUDED */
