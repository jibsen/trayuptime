
Tray Up Time
============

Tray Up Time is a small utility that puts an icon into the system tray,
which displays the system up time when you hover your mouse over it.

It was my entry in the [DonationCoder NANY 2010][1] event.

I also wrote a [blog post][2] about the trouble involved in getting the
system up time on Windows.

[1]: http://www.donationcoder.com/forum/index.php?topic=21213.0
[2]: http://www.hardtoc.com/archives/88
