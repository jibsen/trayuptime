#ifndef RSRC_H_INCLUDED
#define RSRC_H_INCLUDED

#define IDI_MAINICON	500

#define IDD_ABOUT		2000

#define WM_SHELLNOTIFY	(WM_USER + 5)
#define IDI_TRAY		0
#define IDM_ABOUT		1002
#define IDM_EXIT		1010

#define VER_PRODVER		1,0,3,0
#define VER_PRODVER_STR	"1.0.3\0"

#endif /* RSRC_H_INCLUDED */
